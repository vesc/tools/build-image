FROM debian:latest AS proxy
RUN \
  ( \
    timeout 1 bash -c 'cat < /dev/null > /dev/tcp/127.0.0.1/3142' && \
    echo 'Acquire::http { Proxy "http://127.0.0.1:3142"; };' >> /etc/apt/apt.conf.d/01proxy \
  ) || true

FROM proxy AS dep
RUN export DEBIAN_FRONTEND=noninteractive && \
  apt update && \
  apt install -y \
    unzip \
    coreutils \
    make \
    build-essential \
    python \
    git \
    wget \
    libgles2-mesa-dev \
    libdbus-1-dev \
    llvm \
    libclang-dev \
    "^libxkbcommon*" \
    "^libx11*" \
    "^libxcb*"

FROM dep AS build_qt5
RUN git clone https://code.qt.io/qt/qt5.git /usr/src/qt5 && \
  cd /usr/src/qt5 && \
  git checkout 5.12 && \
  perl init-repository --module-subset=default,-qtwebkit,-qtwebkit-examples,-qtwebengine
RUN cd /usr/src/qt5 && \
  sed -i '1s/^/#include <cstdint>\n/' qtlocation/src/3rdparty/mapbox-gl-native/include/mbgl/util/convert.hpp && \
  mkdir -p build && \
  cd build && \
  ../configure -prefix /opt/qt -release -opensource -confirm-license -static -no-sql-mysql -no-sql-psql -no-sql-sqlite -no-journald -qt-zlib -no-mtdev -no-gif -qt-libpng -qt-libjpeg -qt-harfbuzz -qt-pcre -no-glib -no-compile-examples -no-cups -no-iconv -no-tslib -dbus-linked -no-xcb-xlib -no-eglfs -no-directfb -no-linuxfb -no-kms -nomake examples -nomake tests -skip qtwebsockets -skip qtwebchannel -skip qtwebengine -skip qtwayland -skip qtwinextras -skip qtsensors -skip multimedia -no-evdev -no-libproxy -no-icu -no-accessibility -qt-freetype -skip qtimageformats -opengl es2 && \
  make -j$(nproc) && \
  make install
ENV PATH="/opt/qt/bin:${PATH}"
